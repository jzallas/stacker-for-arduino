struct State{
  int block_size;
  int start;
  
  State(){
    block_size = 0;
    start = 0;
  }
  
  void saveCurrentState(int curr_size, int curr_start){
    block_size = curr_size;
    start = curr_start;
  }
  
  int getPrevStateSize(){
    return block_size;
  }
  
  int getPrevStateStart(){
    return start;
  }
};
