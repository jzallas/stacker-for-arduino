/*
  Stacker game using LOL Shield for Arduino
  Copyright 2013 Jonathan Zallas <jzallas@hunter.cuny.edu>
  */


/***** LoL Shield libraries *****/
#include "Charliplexing.h"
#include "Font.h"
/***** https://code.google.com/p/lolshield/ *****/

/***** library that allows every pin to be reassigned as an interrupt *****/
#include <PinChangeInt.h>
/***** https://code.google.com/p/arduino-pinchangeint/ *****/

/***** tone library that uses 16bit timer1 for tones instead of 8bit timer2 *****/
#include <NewTone.h>
/***** https://code.google.com/p/arduino-new-tone/ *****/

#include "Arduino.h"
#include "pitches.h"
#include "State.h"

/* -----------------------------------------------------------------  */
/** SOUND RELATED VARIABLES AND FUNCTIONS
 */
int piezo = A4;
void soundMiss(){
  NewTone(piezo, 125);
  delay(300);
  noNewTone(piezo);
  delay(30);
  NewTone(piezo, 125);
  delay(300);
  noNewTone(piezo);
}

void soundHit(){
  NewTone(piezo, 329);
  delay(120);
  noNewTone(piezo);
}

void soundWinSmall(){  
  NewTone(piezo, 329);
  delay(180);
  noNewTone(piezo);
  delay(50);
  NewTone(piezo, 349);
  delay(80);
  noNewTone(piezo);
  delay(30);
  NewTone(piezo, 392);
  delay(80);
  noNewTone(piezo);
  delay(120);
  NewTone(piezo, 523);
  delay(100);
  noNewTone(piezo);
}

int melody[] = {  //Define the melody as being the notes following using those defined in pitches.h
NOTE_G3, NOTE_C4, NOTE_E4, NOTE_G4, NOTE_C5, 
NOTE_E5, NOTE_G5, NOTE_E5,   0,     NOTE_GS3, 
NOTE_C4, NOTE_DS4, NOTE_GS4, NOTE_C5, NOTE_DS5, 
NOTE_GS5, NOTE_DS5,    0,    NOTE_AS3, NOTE_D4, 
NOTE_F4, NOTE_AS4, NOTE_D5, NOTE_F5, NOTE_AS5, 
NOTE_AS5, NOTE_AS5, NOTE_AS5, NOTE_C6};

int noteDurations[] = {                                               //Define the note durations, 1 to 1 with melody    1 = 8 beats 
  8,8,8,8,8,                                                          //                                                 2 = 4 beats (whole note)
  8,3,4,6,8,                                                          //                                                 4 = 2 beats (half note)
  8,8,8,8,8,                                                          //                                                 8 = 1 beats (quarter note)
  3,4,6,8,8,
  8,8,8,8,3,
  8,8,8,2};
  
void soundWinBig(){
  for (int thisNote = 0; thisNote < 29; thisNote++) {

    int noteDuration = 1000/noteDurations[thisNote];
    NewTone(piezo, melody[thisNote],noteDuration);
    
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    
    noNewTone(piezo);
  }
}


/* -----------------------------------------------------------------  */
/** MOTOR RELATED VARIABLES AND FUNCTIONS
 */
int motor = A2;

void vibrateMiss(){
  digitalWrite(motor, HIGH);
  delay(50);
  digitalWrite(motor, LOW);
  delay(50);
  digitalWrite(motor, HIGH);
  delay(50);
  digitalWrite(motor, LOW);
}

void vibrateHit(){
  digitalWrite(motor, HIGH);
  delay(50);
  digitalWrite(motor, LOW);
}

void vibrateOn(){
  digitalWrite(motor, HIGH);
}

void vibrateOff(){
  digitalWrite(motor, LOW);
}

/* -----------------------------------------------------------------  */
/** SCREEN RELATED VARIABLES AND FUNCTIONS
 */
 
volatile boolean showtext = true;
int k = 0;
static const char test[]="PRESS BUTTON TO START!   ";
int x_min, y_min = 0;
int x_max = 8;
int y_max = 13;

void clearLine(int line){
  for (int i = 0; i <= x_max; i++)
      LedSign::Set(line, i, 0);
}

void clearScreen(){
  for (uint8_t y=0;y<=13;y++) {
    LedSign::Vertical(13-y, 0);
    delay(100);
  }
}
/* -----------------------------------------------------------------  */
/** GAME LOGIC
 */
State state[14];
int current_line = 0;

//14 LEVELS OF DIFFICULTY
int difficulty[] = {250,225,200,175,150,150,125,125,100,100,75,50,40,30};

int block_size=3;
volatile int block_start = 0;
boolean direction = true; // true is right, false is left



void drawBlock(int line){
  for (int i = block_start; i < block_start + block_size; i++){
        LedSign::Set(line,i,1);
      }
}

void playLine(int line, int difficulty)
{
  clearLine(line);
  drawBlock(line);
  delay(difficulty); 
    if (direction){
      if(block_start < x_max+1-block_size) block_start++;
      else{ 
        direction = false;
        block_start--;
      }
    }else
    {
      if(block_start > x_min) block_start--;
      else{
        direction = true;
        block_start++;
      }
    }
}

void exitGame(){
  clearScreen();
  current_line = -1;
  block_size = 3;
  showtext = true;
}

int checkIfAnyLoss()//checks to see if block_size needs to change
{
  int i = current_line;
  int blocks_left = state[i].block_size;
  if ( state[i].start < state[i-1].start){
    //it was pressed to early, calculate loss
    blocks_left = state[i].start + state[i].block_size - state[i-1].start;
    }
  else if (state[i].start > state[i-1].start){
    //it was pressed too late, calculate loss
    blocks_left = state[i-1].start + state[i-1].block_size - state[i].start;
   }
  //if it wasnt pressed too early or too late then it landed on top of the stack
  
  if (blocks_left < 0) blocks_left = 0;
    return blocks_left;
}

void re_drawOnMiss(int new_size){
  int i = current_line;
  
  
  if ( state[i].start < state[i-1].start){
    state[i].start = state[i-1].start;
    state[i].block_size = new_size;
    clearLine(current_line);   
    for (int k = state[i].start; k < state[i].start + new_size; k++){
        LedSign::Set(current_line,k,1);
      }
  }
  else{
    state[i].block_size = new_size;
    clearLine(current_line);   
    for (int k = state[i].start; k < state[i].start + new_size; k++){
        LedSign::Set(current_line,k,1);
      }
  }
  
}

 /* -----------------------------------------------------------------  */
/** BUTTON RELATED VARIABLES AND FUNCTIONS
 */
int button = A0;
volatile boolean buttonPressed = false;
void buttonPress(){
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200) 
  {
    if (showtext){
      clearScreen();
      showtext = false;
    }
    else{
      buttonPressed = true;
      state[current_line].saveCurrentState(block_size, block_start);
    }
  }
  last_interrupt_time = interrupt_time;
  
}

/* -----------------------------------------------------------------  */
/** MAIN program Setup
 */
void setup()                    // run once, when the sketch starts
{
  pinMode(motor, OUTPUT);
  PCintPort::attachInterrupt(button, &buttonPress, RISING);
  LedSign::Init();
  
}



/* -----------------------------------------------------------------  */
/** MAIN program Loop
 */

void loop()                     // run over and over again
{
  
  //THIS WILL SHOW 'PRESS BUTTON TO START' AS LONG AS SHOWTEXT IS TRUE  
  for (int8_t x=DISPLAY_COLS, k=0; showtext==true; x--) {
	LedSign::Clear();
        for (int8_t x2=x, i2=k; x2<DISPLAY_COLS;) {
	    int8_t w = Font::Draw(test[i2], x2, 0);
	    x2 += w, i2 = (i2+1)%strlen(test);
	    if (x2 <= 0)	// off the display completely?
		x = x2, k = i2;
	}
        delay(80);
    }
    
    //if button was pressed check states and prepare to move to next line if possible
    if (buttonPressed){
      if (current_line !=0){
        int new_block_size = checkIfAnyLoss();
        if (new_block_size == 0){ //if there are no more blocks, you lost
          vibrateMiss();
          soundMiss();
          exitGame();
        }
        else{
          if (block_size == new_block_size && !(current_line == y_max-3 || current_line == y_max)){
          //no change in blocksize
          vibrateHit();
          soundHit();
          }
          else if (!(current_line == y_max-3 || current_line == y_max))
          {
            //change in blocksize
            vibrateMiss();
            soundMiss();
            re_drawOnMiss(new_block_size);
            delay(10);
          }
          block_size = new_block_size; // assign new blocksize
          if (current_line == y_max-3){ //consolation prize
            vibrateOn();
            soundWinSmall();
            re_drawOnMiss(block_size);
            vibrateOff();
          }
          else if (current_line == y_max){ //grand prize
            vibrateOn();
            soundWinBig();
            vibrateOff();
            exitGame();
          }
        }
      }
      else{
        vibrateHit(); 
        soundHit();
      }
      current_line++;
      buttonPressed = false;
    }
    
    //move block
    if (!showtext) playLine(current_line, difficulty[current_line]);
      
}
