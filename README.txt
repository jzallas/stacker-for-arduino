APPLICATION NAME
Stacker for Arduino

VERSION
1.1

DESCRIPTION
This is a simple project using an Arduino to play the game called Stacker. It was built for an Arduino uno but theres no reason why it can't be modified to run on any microcontroller that is equal or faster. You can find the schematics as an image labled 'schematic.png'

PARTS LIST
Arduino Uno R3 x 1
LoL Shield v1.5 x 1
1N4007 Flyback Diode x 1
220 Ω Resistor x 1
BC547 NPN Transistor x 1
DC Vibration Motor x 1
Push Button x 1
PKM17EEP-4001-BO Piezo Capsule x 1

EXTERNAL LIBRARIES
LoL Shield - https://code.google.com/p/lolshield/
NewTone Library - https://code.google.com/p/arduino-new-tone/
PinChangeInt Library - https://code.google.com/p/arduino-pinchangeint/


CREDITS
Lead Developer and Architect - Jonathan Zallas

KNOWN BUGS
LoL Shield refresh rate sometimes causes a failure to clear screen on start game. If you keep on playing, the image will eventually adjust itself.